#!/usr/bin/python3
# -*- coding: UTF-8 -*-


#Start by connecting to a particular EC2 region. For Oregon
#the following will do it:

import boto.ec2

conn = boto.ec2.connect_to_region('us-west-2')

# create security group for http and ssh
# Note that ‘0.0.0.0/0’ indicates authorization of traffic from any IP address
print("Do you wish to create a security group ?")
security = input("y/n : ")
if security == 'y':
    secgroup = conn.create_security_group('httpssh', 'Only HTTP and SSH')
    secgroup.authorize('tcp', 80, 80, '0.0.0.0/0')  #HTTP
    secgroup.authorize('tcp', 22, 22, '0.0.0.0/0')  #SSH

#create	the	instance.You use the run_instances() method	to launch an instance

reservation = conn.run_instances('ami-7172b611',key_name ='mint',instance_type ='t2.micro',security_groups=['httpssh'])

