#!/usr/bin/python3
# -*- coding: UTF-8 -*-


#Start by connecting to a particular EC2 region. For Oregon
#the following will do it:

import boto.ec2

conn = boto.ec2.connect_to_region('us-west-2')

# create security group for http and ssh
# Note that ‘0.0.0.0/0’ indicates authorization of traffic from any IP address

# secgroup = conn.create_security_group('httpssh', 'Only HTTP and SSH')
# secgroup.authorize('tcp', 80, 80, '0.0.0.0/0')  #HTTP
# secgroup.authorize('tcp', 22, 22, '0.0.0.0/0')  #SSH

#create	the	instance.You use the run_instances() method	to launch an instance

reservation = conn.run_instances('ami-7172b611',key_name ='Mint',instance_type ='t2.micro',security_groups=['httpssh'])

instance = reservation.instances[0]

instance.add_tag('Name','My first API instance')

#This is required after	changes	are	made to	ensure that	the	boto API has access	to current information

instance.update()

# You can retrieve various instance attributes and assign to variable

print(instance.id)
print(instance.state)
print(instance.ip_address)

# You can also stop, start,	terminate instances.The	following stops	and	restarts the current one:
# instance.stop()
# instance.update()
# instance.start()

# You can alternatively	work with several instances	at once
reservations = conn.get_all_reservations()
reservations
#(shows	a list of all reservations)Particular instance objects can be extracted	from this
inst = reservations[0].instances[0]

#if __name__ == '__main__':
#    main()
