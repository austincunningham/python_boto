#!/usr/bin/python3

import subprocess
#subprocess.call runs OS commands
# list all processes, search for mysql but exclude the grep in the response
output = subprocess.call('ps -A | grep mysql | grep -v grep', shell=True)
# 0 is a successfull response from the command
if output == 1 or output == 2:
    print ("MYSQL NOT RUNNING")
else:
    print ("MYSQL RUNNING")
