#!/usr/bin/python3
import subprocess

cpu = subprocess.call("top -bn 1 | grep '%Cpu'", shell=True)
print ("CPU command successful if zero : ", cpu)
port80 = subprocess.call("netstat -plnt | grep ':80'", shell=True)
print ("PORT 80 command successful if zero : ", port80)
process_count = subprocess.call('ps -Al |  wc -l', shell=True)
print ("Process count command successful if zero : ", process_count)
memory_stat = subprocess.call('vmstat 1 1', shell=True)
print ("MEMORY count command successful if zero : ", memory_stat)
