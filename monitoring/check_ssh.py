#!/usr/bin/python3
import subprocess

# -*- coding: UTF-8 -*-


#Start by connecting to a particular EC2 region. For Oregon
#the following will do it:

import boto.ec2

conn = boto.ec2.connect_to_region('us-west-2')

# create security group for http and ssh
# Note that ‘0.0.0.0/0’ indicates authorization of traffic from any IP address
print("Do you wish to create a security group ?")
security = input("y/n : ")
if security == 'y':
    secgroup = conn.create_security_group('httpssh', 'Only HTTP and SSH')
    secgroup.authorize('tcp', 80, 80, '0.0.0.0/0')  #HTTP
    secgroup.authorize('tcp', 22, 22, '0.0.0.0/0')  #SSH

#create	the	instance.You use the run_instances() method	to launch an instance
print ("enter pem filename")
pem = input (">:")

print ("Do you wish to start a new instance y/n")
newAws = input(">:")
if newAws == "y":
    reservation = conn.run_instances('ami-7172b611',key_name = pem,instance_type ='t2.micro',security_groups=['httpssh'])

# List connections

print('Opening connection... ')
conn =  boto.ec2.connect_to_region("us-west-2")
reservations = conn.get_all_reservations()

for res in reservations:
    for inst in res.instances:
        if 'Name' in inst.tags:
            print("%s (%s) [%s]" % (inst.tags['Name'], inst.id, inst.state))
        else:
            print("%s [%s]" % (inst.id, inst.state))


print ("Enter publicDNS for ec2(e.g. 52.40.225.44)")
ip = input(">:")

print ("Do you wish to upload check_webserver.py scripts y/n ?")
upload = input(">:")

if upload == "y":
    subprocess.call("scp -i ~/Desktop/"+ pem + ".pem  check_webserver.py ec2-user@" + ip +":~/", shell=True)

print ("Do you wish to run to check_webserver.py y/n ?")
nowrun = input(">:")
if nowrun == "y":
    subprocess.call("ssh -i ~/Desktop/" + pem + ".pem ec2-user@" + ip + " '~/check_webserver.py'", shell=True)


